package gui;

import javafx.scene.layout.FlowPane;

/**
 * Created by David on 10/6/2016.
 */
public class AlphabetPane extends FlowPane{
    public static final String[] alphabet = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S",
                                             "T","U","V","W","X","Y","Z"};
    public static final String boxColor = "#4D69B6";
    public static final String changeColor = "#4A6587";
    public static final String hint = "HINT";

    public AlphabetPane() {
        super();
        this.setHgap(5);
        this.setVgap(5);
        this.setPrefWidth(800);
        addLetters();
    }

    private void addLetters() {
        for (int i = 0; i < 26; i++) {
            LetterBox box = new LetterBox(alphabet[i]);
            this.getChildren().add(box);
        }
    }

    public void updateLetters(String letter) {
        for (int i = 0; i < 26; i++) {
            if (letter.equalsIgnoreCase(alphabet[i])) {
                LetterBox box = (LetterBox) this.getChildren().get(i);
                box.updateLetters(letter);
            }
        }
    }

    public void lost() {
        for (int i = 0; i < 26; i++) {
            LetterBox box = (LetterBox) this.getChildren().get(i);
            box.updateLetters(alphabet[i]);
        }
    }
}
