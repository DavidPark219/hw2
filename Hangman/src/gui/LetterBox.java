package gui;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by David on 10/7/2016.
 */
public class LetterBox extends StackPane{
    public static final String boxColor = "#4D69B6";
    public static final String changeColor = "#4A6587";
    public static final String missedColor = "#c63d3d";

    public LetterBox(String letter) {
        super();
        addLetter(letter);
    }

    public LetterBox(Text letter) {
        super();
        addLetter(letter.getText());
        blankSpace();
    }

    private void addLetter(String letter) {
        Rectangle square = new Rectangle(75, 75);
        square.setFill(Paint.valueOf(boxColor));
        Text letterText = new Text(letter);
        letterText.setFont(Font.font("Helvetica", 25));
        letterText.setFill(Color.WHITE);
        letterText.setBoundsType(TextBoundsType.VISUAL);
        this.getChildren().addAll(square, letterText);
    }

    public void updateLetters(String letter) {
        Rectangle square = (Rectangle) this.getChildren().get(0);
        square.setFill(Paint.valueOf(changeColor));
        square.setOpacity(.75);
    }

    public void blankSpace() {
        Rectangle square = (Rectangle) this.getChildren().get(0);
        Text letterText = (Text) this.getChildren().get(1);
        square.setWidth(20);
        square.setHeight(30);
        square.setFill(Color.TRANSPARENT);
        square.setStroke(Color.BLACK);
        letterText.setFont(Font.font("Helvetica", 20));
        letterText.setBoundsType(TextBoundsType.LOGICAL_VERTICAL_CENTER);
        letterText.setFill(Color.BLACK);
        letterText.setVisible(false);
    }

    public void setLetterVisible(boolean visible) {
        if (visible) {
            Text letterText = (Text) this.getChildren().get(1);
            letterText.setVisible(true);
            letterText.setFill(Color.WHITE);
            Rectangle square = (Rectangle) this.getChildren().get(0);
            square.setFill(Paint.valueOf(boxColor));
        }
    }

    public boolean isLetterVisible() {
        Text letterText = (Text) this.getChildren().get(1);
        return letterText.isVisible();
    }

    public void setMissedLetter(String letter) {
        Text letterText = (Text) this.getChildren().get(1);
        letterText.setVisible(true);
        letterText.setFill(Color.WHITE);
        Rectangle square = (Rectangle) this.getChildren().get(0);
        square.setFill(Paint.valueOf(missedColor));
    }

    public String getLetter() {
        Text letterText = (Text) this.getChildren().get(1);
        return letterText.getText();
    }
}
