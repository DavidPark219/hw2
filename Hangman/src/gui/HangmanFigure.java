package gui;


import javafx.geometry.Insets;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;

/**
 * Created by David on 10/8/2016.
 */
public class HangmanFigure extends Pane {
    public static final String boxColor = "#4D69B6";
    private int counter;

    public HangmanFigure() {
        super();
        Insets padding = new Insets(5,20,5,20);
        this.setPadding(padding);
        createFigure();
        counter = 0;
    }

    public void createFigure() {
        Line base = new Line(25, 250, 275, 250);
        base.setStroke(Paint.valueOf(boxColor));
        base.setStrokeWidth(2);
        base.setVisible(false);
        this.getChildren().add(base);

        Line pillar = new Line(50, 25, 50, 250);
        pillar.setStroke(Paint.valueOf(boxColor));
        pillar.setStrokeWidth(2);
        pillar.setVisible(false);
        this.getChildren().add(pillar);

        Line top = new Line(50, 25, 175, 25);
        top.setStroke(Paint.valueOf(boxColor));
        top.setStrokeWidth(2);
        top.setVisible(false);
        this.getChildren().add(top);

        Line rope = new Line(175, 25, 175, 70);
        rope.setStroke(Paint.valueOf(boxColor));
        rope.setStrokeWidth(2);
        rope.setVisible(false);
        this.getChildren().add(rope);

        Ellipse head = new Ellipse(175, 95 , 25, 25);
        head.setStroke(Paint.valueOf(boxColor));
        head.setStrokeWidth(2);
        head.setFill(Color.TRANSPARENT);
        head.setVisible(false);
        this.getChildren().add(head);

        Line body = new Line(175, 120, 175, 215);
        body.setStroke(Paint.valueOf(boxColor));
        body.setStrokeWidth(2);
        body.setVisible(false);
        this.getChildren().add(body);

        Line leftArm = new Line(175, 135, 130, 155);
        leftArm.setStroke(Paint.valueOf(boxColor));
        leftArm.setStrokeWidth(2);
        leftArm.setVisible(false);
        this.getChildren().add(leftArm);

        Line rightArm = new Line(175, 135, 220, 155);
        rightArm.setStroke(Paint.valueOf(boxColor));
        rightArm.setStrokeWidth(2);
        rightArm.setVisible(false);
        this.getChildren().add(rightArm);

        Line leftLeg = new Line(175, 215, 150, 235);
        leftLeg.setStroke(Paint.valueOf(boxColor));
        leftLeg.setStrokeWidth(2);
        leftLeg.setVisible(false);
        this.getChildren().add(leftLeg);

        Line rightLeg = new Line(175, 215, 200, 235);
        rightLeg.setStroke(Paint.valueOf(boxColor));
        rightLeg.setStrokeWidth(2);
        rightLeg.setVisible(false);
        this.getChildren().add(rightLeg);
    }

    public void makeVisible(){
        this.getChildren().get(counter).setVisible(true);
        counter++;
    }
}
